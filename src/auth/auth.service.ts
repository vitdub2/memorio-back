import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../users/user.service';
import { UserDto } from '../users/user.interfaces';
import { JwtService } from '@nestjs/jwt';
import { IToken } from './auth.interfaces';
import { passwordCutter } from '../users/password.cutter';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<UserDto | null> {
    const user = await this.userService.findUserByEmail(email);
    if (user) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        return passwordCutter(user);
      } else {
        throw new UnauthorizedException('Invalid password or email');
      }
    }
    throw new UnauthorizedException('Invalid password or email');
  }

  async login(user: UserDto): Promise<IToken> {
    return {
      token: this.jwtService.sign(user),
    };
  }
}
