import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CatDto } from './cats.interfaces';
import { InjectModel } from '@nestjs/mongoose';
import { Cat, CatDocument } from '../schemas/cat.schema';
import { Model } from 'mongoose';
import { User, UserDocument } from '../schemas/user.schema';

@Injectable()
export class CatsService {
  constructor(
    @InjectModel(Cat.name) private catModel: Model<CatDocument>,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) {}

  async getAllCats(): Promise<Cat[]> {
    return this.catModel.find().exec();
  }

  async getMyCats(owner: string): Promise<Cat[]> {
    return this.catModel.find({ owner }).exec();
  }

  async getCatById(id: string): Promise<Cat> {
    return this.catModel.findById(id).exec();
  }

  async createNewCat(owner: string, catDto: CatDto): Promise<Cat> {
    const cat = new this.catModel(
      Object.assign({
        ...catDto,
        owner,
      }),
    );
    return cat.save();
  }

  async updateCat(userId: string, catId: string, catDto: CatDto): Promise<Cat> {
    const cat = await this.catModel.findById(catId);
    if (cat.owner === userId) {
      return await this.catModel
        .findOneAndUpdate({ _id: catId }, catDto, {
          new: true,
          useFindAndModify: false,
        })
        .exec();
    }
    throw new UnauthorizedException('You cannot edit this cat!');
  }

  async deleteCat(userId: string, catId: string): Promise<Cat> {
    const cat = await this.catModel.findById(catId).exec();
    if (cat.owner === userId) {
      return this.catModel
        .findOneAndDelete(
          { _id: catId },
          {
            new: true,
            useFindAndModify: false,
          },
        )
        .exec();
    } else {
      throw new UnauthorizedException('You cannot delete this cat!');
    }
  }
}
