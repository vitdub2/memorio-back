import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { CatsService } from './cats.service';
import { CatDto } from './cats.interfaces';
import { ValidationPipe } from 'src/utils/validation.pipe';
import { Cat } from '../schemas/cat.schema';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { UserID } from '../utils/user-id.decorator';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { MODULE_NAMES } from '../utils/constants';

@Controller(MODULE_NAMES.CATS)
@ApiTags(MODULE_NAMES.CATS)
export class CatsController {
  constructor(private readonly catService: CatsService) {}

  @Get()
  getCats(): Promise<Cat[]> {
    return this.catService.getAllCats();
  }

  @Get('/my')
  @UseGuards(JwtAuthGuard)
  getMyCats(@UserID() id: string): Promise<Cat[]> {
    return this.catService.getMyCats(id);
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBody({
    type: CatDto,
  })
  createCat(
    @Body(new ValidationPipe()) catDto: CatDto,
    @UserID() owner: string,
  ): Promise<Cat> {
    return this.catService.createNewCat(owner, catDto);
  }

  @Get(':id')
  getCatById(@Param('id') id: string): Promise<Cat> {
    return this.catService.getCatById(id);
  }

  @Put(':id')
  @ApiBody({
    type: CatDto,
  })
  updateCat(
    @UserID() userId: string,
    @Param('id') catId: string,
    @Body(new ValidationPipe()) updateDto: CatDto,
  ): Promise<Cat> {
    return this.catService.updateCat(userId, catId, updateDto);
  }

  @Delete(':id')
  deleteCat(
    @UserID() userId: string,
    @Param('id') catId: string,
  ): Promise<Cat> {
    return this.catService.deleteCat(userId, catId);
  }
}
