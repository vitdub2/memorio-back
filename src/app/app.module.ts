import { Global, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CatsModule } from '../cats/cats.module';
import { configService } from '../utils/config.service';
import { LoggerMiddleware } from '../utils/logger.middleware';
import { CatsController } from '../cats/cats.controller';
import { UserModule } from '../users/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '../auth/auth.module';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { GraphQLModule } from '@nestjs/graphql';
import { UserResolver } from '../users/user.resolver';
import { CalendarModule } from '../calendars/calendar.module';

@Global()
@Module({
  imports: [
    MongooseModule.forRoot(configService.getTypeOrmConfig()),
    CatsModule,
    UserModule,
    AuthModule,
    CalendarModule,
    GraphQLModule.forRoot({
      autoSchemaFile: true,
    }),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    UserResolver,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(LoggerMiddleware).forRoutes(CatsController);
  }
}
