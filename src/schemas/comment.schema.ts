import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type CommentDocument = Comment & mongoose.Document;

@Schema({ timestamps: true })
export class Comment {
  @Prop({ required: true })
  text: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Event' })
  event: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' })
  author: string;

  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: 'User' })
  likedBy: string[];
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
