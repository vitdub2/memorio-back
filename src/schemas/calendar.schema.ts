import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type CalendarDocument = Calendar & mongoose.Document;

@Schema({ timestamps: true })
export class Calendar {
  @Prop({ required: true })
  name: string;

  @Prop()
  description: string;

  @Prop()
  color: string;

  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: 'User' })
  editors: string[];

  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: 'User' })
  participants: string[];
}

export const CalendarSchema = SchemaFactory.createForClass(Calendar);
