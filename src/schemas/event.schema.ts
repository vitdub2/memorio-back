import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type EventDocument = Event & mongoose.Document;

@Schema({ timestamps: true })
export class Event {
  @Prop({ required: true })
  name: string;

  @Prop()
  description: string;

  @Prop()
  place: string;

  @Prop()
  site: string;

  @Prop({ type: mongoose.Schema.Types.Date })
  startDate: string;

  @Prop({ type: mongoose.Schema.Types.Date })
  endDate: string;

  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: 'User' })
  participants: string[];

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Calendar' })
  calendar: string;
}

export const EventSchema = SchemaFactory.createForClass(Event);
