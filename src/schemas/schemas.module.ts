import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './user.schema';
import { Cat, CatSchema } from './cat.schema';
import { Calendar, CalendarSchema } from './calendar.schema';
import { Comment, CommentSchema } from './comment.schema';
import { Event, EventSchema } from './event.schema';
import { Label, LabelSchema } from './label.schema';

const schemas = [
  MongooseModule.forFeature([
    { name: User.name, schema: UserSchema },
    { name: Cat.name, schema: CatSchema },
    { name: Calendar.name, schema: CalendarSchema },
    { name: Comment.name, schema: CommentSchema },
    { name: Event.name, schema: EventSchema },
    { name: Label.name, schema: LabelSchema },
  ]),
];

@Module({
  imports: schemas,
  exports: schemas,
})
export class SchemasModule {}
