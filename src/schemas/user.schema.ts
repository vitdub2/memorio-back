import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Exclude } from 'class-transformer';
import { Field, ObjectType } from '@nestjs/graphql';
import mongoose from 'mongoose';

@Schema({ timestamps: true })
@ObjectType()
export class User {
  @Prop({ required: true })
  @Field()
  firstName: string;

  @Prop({ required: true })
  @Field()
  lastName: string;

  @Prop()
  @Field({ nullable: true })
  patronymic: string;

  @Prop({ required: true, unique: true })
  @Field()
  email: string;

  @Prop({ required: true })
  @Exclude()
  @Field()
  password: string;
}

export type UserDocument = User & mongoose.Document;

export const UserSchema = SchemaFactory.createForClass(User);
