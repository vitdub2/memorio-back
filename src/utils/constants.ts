export const MODULE_NAMES = {
  USERS: 'users',
  CATS: 'cats',
  CALENDARS: 'calendars',
  EVENTS: 'calendar_events',
  COMMENTS: 'comments',
  LABELS: 'labels',
};
