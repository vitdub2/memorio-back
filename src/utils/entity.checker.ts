import { NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

export const checkEntity = async <T>(
  callback: (elem: T & mongoose.Document) => void,
  entityId: string,
  model: Model<T & mongoose.Document>,
): Promise<any> => {
  let entity = null;
  try {
    entity = await model.findById(entityId).exec();
  } finally {
    if (!entity) {
      throw new NotFoundException('Invalid id');
    }
    return callback(entity);
  }
};
