import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { UserCreateDto, UserDto } from './user.interfaces';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../schemas/user.schema';
import { Model } from 'mongoose';
import { passwordCutter } from './password.cutter';
import { map } from 'lodash';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async findAllUsers(): Promise<User[]> {
    return map(await this.userModel.find().exec(), (user) =>
      passwordCutter(user),
    );
  }

  async findUserByEmail(email: string): Promise<User> {
    return this.userModel.findOne({ email });
  }

  async findUserById(id: string): Promise<User> {
    return passwordCutter(await this.userModel.findById(id).exec());
  }

  async createUser(userDto: UserCreateDto): Promise<any> {
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash(userDto.password, salt);
    try {
      const user = await new this.userModel({
        ...userDto,
        password,
      });
      return passwordCutter(await user.save());
    } catch (e) {
      throw new BadRequestException('User with this email already exists');
    }
  }

  async updateUser(id: string, userDto: UserDto): Promise<User> {
    return passwordCutter(
      await this.userModel
        .findOneAndUpdate({ _id: id }, userDto, {
          new: true,
          useFindAndModify: false,
        })
        .exec(),
    );
  }

  async deleteUser(id: string): Promise<User> {
    return passwordCutter(
      await this.userModel
        .findOneAndDelete(
          { _id: id },
          {
            useFindAndModify: false,
          },
        )
        .exec(),
    );
  }
}
