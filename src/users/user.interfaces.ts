import { IsEmail, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
  @IsString()
  @ApiProperty()
  firstName: string;

  @IsString()
  @ApiProperty()
  lastName: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    nullable: true,
  })
  patronymic: string;

  @IsEmail()
  @ApiProperty()
  email: string;
}

export class UserCreateDto extends UserDto {
  @IsString()
  @ApiProperty()
  password: string;
}
