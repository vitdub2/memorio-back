export const passwordCutter = (user: any) => {
  const trueUser = user['_doc'];
  delete trueUser.password;
  return trueUser;
};
