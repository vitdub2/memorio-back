import { Args, Int, Query, Resolver } from '@nestjs/graphql';
import { User } from '../schemas/user.schema';
import { UserService } from './user.service';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '../auth/gql.auth.guard';
import { Public } from '../utils/public.decorator';

@Resolver((of) => User)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Public()
  @UseGuards(GqlAuthGuard)
  @Query((returns) => User, { name: 'getUserById' })
  async user(@Args('id') id: string) {
    return await this.userService.findUserById(id);
  }

  @Public()
  @UseGuards(GqlAuthGuard)
  @Query((returns) => [User], { name: 'getAllUsers' })
  async users() {
    return await this.userService.findAllUsers();
  }
}
