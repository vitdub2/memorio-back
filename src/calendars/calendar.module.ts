import { Module } from '@nestjs/common';
import { CalendarController } from './calendar.controller';
import { CalendarService } from './calendar.service';
import { SchemasModule } from '../schemas/schemas.module';
import { EventController } from './event.controller';

@Module({
  imports: [SchemasModule],
  providers: [CalendarService],
  controllers: [EventController, CalendarController],
  exports: [CalendarService],
})
export class CalendarModule {}
