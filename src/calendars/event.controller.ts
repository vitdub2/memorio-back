import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { MODULE_NAMES } from '../utils/constants';
import { ApiTags } from '@nestjs/swagger';
import { CalendarService } from './calendar.service';
import { UserID } from '../utils/user-id.decorator';
import { Event } from '../schemas/event.schema';
import { ValidationPipe } from '../utils/validation.pipe';
import { EventDto } from './calendar.interfaces';

@Controller(MODULE_NAMES.CALENDARS)
@ApiTags(MODULE_NAMES.CALENDARS)
export class EventController {
  constructor(private readonly calendarService: CalendarService) {}

  @Get('events')
  getUserEvents(@UserID() userId: string): Promise<Event[]> {
    return this.calendarService.getUserEvents(userId);
  }

  @Get('events/:eventId')
  getEvent(
    @UserID() userId: string,
    @Param('eventId') eventId: string,
  ): Promise<Event[]> {
    return this.calendarService.getUserEvents(userId);
  }

  @Post(':id/events')
  createNewEvent(
    @UserID() userId: string,
    @Body(new ValidationPipe()) eventDto: EventDto,
    @Param('id') calendarId: string,
  ): Promise<Event> {
    return this.calendarService.createEvent(eventDto, calendarId, userId);
  }

  @Put(':calendarId/events/:eventId')
  updateEvent(
    @Param('calendarId') calendarId: string,
    @Param('eventId') eventId: string,
    @Body(new ValidationPipe()) eventDto: EventDto,
    @UserID() userId: string,
  ): Promise<Event> {
    return this.calendarService.updateEvent(
      eventDto,
      userId,
      eventId,
      calendarId,
    );
  }

  @Delete(':calendarId/events/:eventId')
  deleteEvent(
    @Param('calendarId') calendarId: string,
    @Param('eventId') eventId: string,
    @UserID() userId: string,
  ): Promise<Event> {
    return this.calendarService.deleteEvent(userId, eventId, calendarId);
  }
}
