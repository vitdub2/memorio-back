import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { MODULE_NAMES } from '../utils/constants';
import { UserID } from '../utils/user-id.decorator';
import { Calendar } from '../schemas/calendar.schema';
import { CalendarService } from './calendar.service';
import { ValidationPipe } from '../utils/validation.pipe';
import { CalendarDto, EventDto } from './calendar.interfaces';
import { Event } from '../schemas/event.schema';

@Controller(MODULE_NAMES.CALENDARS)
@ApiTags(MODULE_NAMES.CALENDARS)
export class CalendarController {
  constructor(private readonly calendarService: CalendarService) {}
  @Get()
  getCalendars(@UserID() userId: string): Promise<Calendar[]> {
    return this.calendarService.getAllCalendars(userId);
  }

  @Get(':id')
  getCalendarById(
    @UserID() userId: string,
    @Param('id') calendarId: string,
  ): Promise<Calendar> {
    return this.calendarService.getCalendarById(calendarId, userId);
  }

  @Post()
  createNewCalendar(
    @Body(new ValidationPipe()) calendarDto: CalendarDto,
    @UserID() userId: string,
  ): Promise<Calendar> {
    return this.calendarService.createNewCalendar(calendarDto, userId);
  }

  @Put(':id')
  updateCalendar(
    @Param('id') calendarId: string,
    @Body(new ValidationPipe()) calendarDto: CalendarDto,
    @UserID() userId: string,
  ): Promise<Calendar> {
    return this.calendarService.updateCalendar(calendarDto, userId, calendarId);
  }

  @Delete(':id')
  deleteCalendar(
    @Param('id') calendarId: string,
    @UserID() userId: string,
  ): Promise<Calendar> {
    return this.calendarService.deleteCalendar(calendarId, userId);
  }
}
