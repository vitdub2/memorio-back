import {
  Injectable,
  NotAcceptableException,
  UnauthorizedException,
} from '@nestjs/common';
import { Calendar, CalendarDocument } from '../schemas/calendar.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CalendarDto, EventDto } from './calendar.interfaces';
import { compact, uniq, concat, map } from 'lodash';
import { checkEntity } from '../utils/entity.checker';
import { Event, EventDocument } from '../schemas/event.schema';

@Injectable()
export class CalendarService {
  constructor(
    @InjectModel(Calendar.name)
    private readonly calendarModel: Model<CalendarDocument>,
    @InjectModel(Event.name) private readonly eventModel: Model<EventDocument>,
  ) {}
  getAllCalendars(userId: string): Promise<Calendar[]> {
    return this.calendarModel
      .find({ $or: [{ editors: [userId] }, { participants: [userId] }] })
      .exec();
  }

  private async checkCalendar(callback: any, calendarId: string): Promise<any> {
    return checkEntity(callback, calendarId, this.calendarModel);
  }

  async getCalendarById(calendarId: string, userId: string): Promise<Calendar> {
    return await this.checkCalendar((calendar) => {
      if (
        calendar.editors.includes(userId) ||
        calendar.participants.includes(userId)
      ) {
        return calendar;
      } else {
        throw new NotAcceptableException('You can`t get this calendar');
      }
    }, calendarId);
  }

  createNewCalendar(
    calendarDto: CalendarDto,
    authorId: string,
  ): Promise<Calendar> {
    const trueDto = {
      ...calendarDto,
      editors: uniq([...compact(calendarDto.editors), authorId]),
    };

    return new this.calendarModel(trueDto).save();
  }

  async updateCalendar(
    calendarDto: CalendarDto,
    authorId: string,
    calendarId: string,
  ): Promise<Calendar> {
    return await this.checkCalendar((calendar) => {
      if (calendar.editors.includes(authorId)) {
        const trueDto = {
          ...calendarDto,
          editors: uniq([...compact(calendarDto.editors), authorId]),
        };
        return this.calendarModel
          .findOneAndUpdate({ _id: calendarId }, trueDto, {
            new: true,
            useFindAndModify: false,
          })
          .exec();
      } else {
        throw new UnauthorizedException('You cannot edit this calendar');
      }
    }, calendarId);
  }

  async deleteCalendar(calendarId: string, userId: string): Promise<Calendar> {
    return await this.checkCalendar((calendar) => {
      if (calendar.editors.includes(userId)) {
        return this.calendarModel
          .findOneAndDelete(
            { _id: calendarId },
            {
              new: true,
              useFindAndModify: false,
            },
          )
          .exec();
      } else {
        throw new UnauthorizedException('You cannot delete this calendar');
      }
    }, calendarId);
  }

  private async checkEvent(callback: any, eventId: string) {
    return checkEntity(callback, eventId, this.eventModel);
  }

  async getUserEvents(userId: string): Promise<Event[]> {
    const calendars = await this.getAllCalendars(userId);

    const calendarsIds: string[] = map(
      calendars,
      (calendar: CalendarDocument) => calendar._id,
    );

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    //@ts-ignore
    return this.eventModel.find({ calendar: [...calendarsIds] }).exec();
  }

  async getEvent(userId: string, eventId: string): Promise<Event> {
    const event = await this.eventModel.findById(eventId).exec();
    if (event.participants.includes(userId)) {
      return event;
    }
    this.checkCalendar((calendar) => {
      if (calendar.editors.includes(userId)) {
        return event;
      } else {
        throw new NotAcceptableException('You can`t get this event');
      }
    }, event.calendar);
  }

  async createEvent(
    eventDto: EventDto,
    calendarId: string,
    userId: string,
  ): Promise<Event> {
    const event = await new this.eventModel({
      ...eventDto,
      calendar: calendarId,
    });
    const calendar = await this.getCalendarById(calendarId, userId);
    if (calendar) {
      return event.save();
    } else {
      event.delete();
    }
  }

  async updateEvent(
    eventDto: EventDto,
    userId: string,
    eventId: string,
    calendarId: string,
  ): Promise<Event> {
    const calendar = await this.getCalendarById(calendarId, userId);
    if (calendar) {
      return this.checkEvent(() => {
        return this.eventModel.findOneAndUpdate(
          { _id: eventId },
          { ...eventDto, calendar: calendarId },
          {
            new: true,
            useFindAndModify: false,
          },
        );
      }, eventId);
    }
  }

  async deleteEvent(
    userId: string,
    eventId: string,
    calendarId: string,
  ): Promise<Event> {
    return this.checkEvent(async (event) => {
      const calendar = await this.getCalendarById(calendarId, userId);
      if (calendar) {
        event.delete();
        return event;
      }
    }, eventId);
  }
}
