import { IsArray, IsString } from 'class-validator';
import { Optional } from '@nestjs/common';

export class CalendarDto {
  @IsString()
  name: string;

  @Optional()
  @IsString()
  description: string;

  @Optional()
  @IsString()
  color: string;

  @Optional()
  editors: string[];

  @Optional()
  participants: string[];

  @Optional()
  events: string[];
}

export class EventDto {
  @IsString()
  name: string;

  @Optional()
  @IsString()
  description: string;

  @Optional()
  @IsString()
  place: string;

  @Optional()
  @IsString()
  site: string;

  @Optional()
  @IsString()
  startDate: string;

  @Optional()
  @IsString()
  endDate: string;

  @Optional()
  @IsArray()
  participants: string[];
}
